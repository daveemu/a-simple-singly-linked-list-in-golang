package list

import "fmt"

type Node struct {
	value int64
	next  *Node
}

type List struct {
	head *Node
	tail *Node
}

func CreateList() *List {
	l := new(List)
	l.head = nil
	l.tail = nil
	return l
}

func (l *List) Prepend(v int64) *List {
	n := new(Node)
	n.value = v
	n.next = l.head
	l.head = n

	if l.tail == nil {
		l.tail = n
	}

	return l
}

func (l *List) Append(v int64) *List {
	n := new(Node)
	n.value = v
	n.next = nil

	if l.tail == nil {
		l.tail = n
	} else {
		l.tail.next = n
	}

	if l.head == nil {
		l.head = n
	}

	return l
}

func (l *List) Print() {
	var n *Node
	for n = l.head; n != nil; n = n.next {
		fmt.Printf("%d -> ", n.value)
	}
	fmt.Printf("\n")
}

func (l *List) Contains(v int64) bool {
	var n *Node
	for n = l.head; n != nil; n = n.next {
		if n.value == v {
			return true
		}
	}
	return false
}

func (l *List) Remove(v int64) bool {
	var curr *Node = l.head
	var prev *Node = nil

	for curr != nil && curr.value != v {
		prev = curr
		curr = curr.next
	}

	if curr == nil {
		return false
	}

	if curr == l.tail {
		l.tail = prev
	}

	if curr == l.head {
		l.head = curr.next
	}

	if prev != nil {
		prev.next = curr.next
	}

	return true
}
