package main

import (
	"list"
)

func main() {

	myList := list.CreateList()
	myList2 := list.CreateList()

	myList.Append(13)
	myList.Append(14)
	myList.Prepend(15)

	myList2.Prepend(17)
	myList2.Prepend(18)
	myList2.Append(69)

	myList.Print()
	myList2.Print()

	myList.Remove(13)
	myList2.Remove(69)

	myList.Print()
	myList2.Print()

}
